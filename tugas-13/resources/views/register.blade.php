@extends('layout.master')

@section('judul')
    Halaman Registrasi
@endsection

@section('content')
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome" method="POST">
        @csrf
        <p>First Name:</p>
        <input type="text" name="nama_depan">

        <p>Last Name:</p>
        <input type="text" name="nama_belakang">

        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label>

        <p>Nationality:</p>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select>

        <p>Language Spoken:</p>
        <input type="checkbox" id="bahasa" name="bahasa" value="Bahasa">
        <label for="bahasa"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="english" value="English">
        <label for="english"> English</label><br>
        <input type="checkbox" id="other" name="other" value="Other">
        <label for="other"> Other</label>

        <p>Bio:</p>
        <textarea cols="30" rows="10"></textarea>

        <br>
        <button type="submit">Sign Up</button>
    </form>
@endsection
