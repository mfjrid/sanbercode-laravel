<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        $namadepan = $request['nama_depan'];
        $namabelakang = $request['nama_belakang'];
        return view('welcome', [
            'nama_depan' => $namadepan,
            'nama_belakang' => $namabelakang
        ]);
    }
}
